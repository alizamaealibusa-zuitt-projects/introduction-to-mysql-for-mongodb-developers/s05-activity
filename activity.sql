USE classic_models;

	-- 1
	SELECT customerName FROM customers WHERE country = "Philippines";

	-- 2
	SELECT contactLastName, contactFirstName FROM customers WHERE customerName = "La Rochelle Gifts";

	-- 3
	SELECT productName, MSRP FROM products WHERE productName = "The Titanic";

	-- 4
	SELECT firstName, lastName FROM employees WHERE email = "jfirrelli@classicmodelcars.com";

	-- 5
	SELECT customerName FROM customers WHERE state IS NULL;

	-- 6
	SELECT firstName, lastName, email FROM employees WHERE lastName = "Patterson" AND firstName = "Steve";

	-- 7
	SELECT customerName, country, creditLimit FROM customers WHERE country != "USA" AND creditLimit > 3000;

	-- 8
	SELECT customerName FROM customers WHERE customerName NOT LIKE "%a%";

	-- 9
	SELECT customerNumber FROM orders WHERE comments LIKE "%DHL%";

	-- 10
	SELECT productLine FROM productlines WHERE textDescription LIKE "%state of the art%";

	-- 11
	SELECT DISTINCT country FROM customers;

	-- 12
	SELECT DISTINCT status FROM orders;

	-- 13
	SELECT DISTINCT customerName, country FROM customers WHERE country = "USA" OR country = "France" OR country = "Canada";

	-- 14
	SELECT employees.firstName, employees.lastName, offices.city FROM employees JOIN offices ON employees.officeCode = offices.officeCode WHERE offices.city = "Tokyo";

	-- 15
	SELECT customerName FROM customers WHERE salesRepEmployeeNumber = 1166;

	-- 16
	SELECT products.productName, customers.customerName FROM customers JOIN orders ON customers.customerNumber = orders.customerNumber JOIN orderdetails ON orders.orderNumber = orderdetails.orderNumber JOIN products ON orderdetails.productCode = products.productCode WHERE customers.customerName = "Baane Mini Imports";

	-- 17
	SELECT DISTINCT employees.firstName, employees.lastName, customers.customerName, offices.country FROM payments JOIN customers ON payments.customerNumber = customers.customerNumber JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber JOIN offices ON employees.officeCode = offices.officeCode WHERE customers.country = offices.country;

	-- 18
	SELECT firstName, lastName FROM employees WHERE reportsTo = 1143;

	-- 19
	SELECT productName, MAX(MSRP) FROM products;

	-- 20
	SELECT country, COUNT(*) FROM customers WHERE country = "UK";

	-- 21
	SELECT productLine, COUNT(*) FROM products GROUP BY productLine;

	-- 22
	SELECT salesRepEmployeeNumber, COUNT(*) FROM customers GROUP BY salesRepEmployeeNumber;

	-- 23
	SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;